const mongoose = require("mongoose");
const Tree = require("./tree");

const missionSchema = new mongoose.Schema({
  id: String,
  name: String,
  executed: Boolean,
  date: String,
  images: [
    {
      image: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Image",
      },
    },
  ],
  path: {
    name: String,
    trees: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tree",
      },
    ],
  },
});

const Mission = mongoose.model("Mission", missionSchema);

module.exports = Mission;
