const Mission = require("../models/mission");
const moment = require("moment");
const { dateFormat } = require("../utils/constants");

exports.getMission = async function (id) {
  const mission = await Mission.findOne({ id: id })
    .populate("path.trees")
    .populate("images.image")
    .exec();
  if (mission) {
    return mission;
  } else {
    throw new Error("Mission does not exists");
  }
};

exports.searchAll = async function () {
  let missions = await Mission.find({})
    .populate("path.trees")
    .populate("images.image")
    .exec();
  if (missions) {
    return missions;
  }
};

exports.create = async function (
  missionId,
  missionName,
  pathName,
  pathTrees,
  date
) {
  let mission = await Mission.findOne({ id: missionId }).exec();
  if (!mission) {
    mission = new Mission({
      id: missionId,
      name: missionName,
      date: moment(date).format(dateFormat),
      excecuted: false,
      path: {
        name: pathName,
        trees: pathTrees,
      },
    });
    mission = await mission.save();
  } else {
    throw new Error("Mission already exists");
  }

  return mission;
};

exports.execute = async function (missionId) {
  const mission = await Mission.findOne({ id: missionId }).exec();

  if (mission) {
    mission.executed = true;
    return await mission.save();
  } else {
    throw new Error("Mission not exists");
  }
};
