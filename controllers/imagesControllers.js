const imageService = require("../services/imagesService");

exports.upload = async function (req, res) {
  try {
    const missionId = req.params["id"];
    const imageId = req.body["imageId"];
    const lat = req.body["latitude"];
    const lon = req.body["longitude"];
    const imageFile = req.files.image;

    const mission = await imageService.upload(
      missionId,
      imageId,
      imageFile,
      lat,
      lon
    );

    res.send(mission);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.updateScore = async function (req, res) {
  try {
    const imageId = req.params["id"];
    const score = req.params["score"];
    const details = req.body["details"];
    const tags = req.body["tags"];

    const image = await imageService.score(imageId, score, details, tags);

    if (!image) {
      res.status(400).send({ message: `Image ${imageId} not found` });
    } else {
      res.send(image);
    }
  } catch (err) {
    //Mongoose validation errors
    if (err.errors) {
      res.status(400).send({ message: err.message });
    } else {
      res.status(500).send(err.message);
    }
  }
};
