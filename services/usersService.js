const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

exports.search = async function (userId) {
  const user = await User.findOne({ _id: userId }).exec();

  if (user) {
    return user;
  }
};

exports.searchAll = async function () {
  const user = await User.find({}).exec();

  if (user) {
    return user;
  }
};

exports.delete = async function (userId) {
  const user = await User.deleteOne({ _id: userId }).exec();

  if (user) {
    return user;
  }
};

exports.updateEmail = async function (userId, newEmail) {
  const user = await User.findOneAndUpdate(
    { _id: userId },
    { $set: { email: newEmail } },
    { new: true }
  );

  return user;
};

exports.signUp = async function (userData) {
  const { userName, userSurname, userEmail, userPassword } = userData;

  const userExists = await User.findOne({ email: userEmail }).exec();
  if (!userExists) {
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(userPassword, salt);

    const user = new User({
      name: userName,
      surname: userSurname,
      email: userEmail,
      password: hashPassword,
    });

    const savedUser = await user.save();

    const signResponse = new Promise((resolve, reject) => {
      jwt.sign(
        { user: userEmail },
        process.env.NODE_ENV_SECRET_KEY,
        (error, token) => {
          if (error) reject(error.message);
          resolve({
            user: savedUser,
            token,
          });
        }
      );
    });

    return await signResponse;
  } else {
    throw new Error("User already exists");
  }
};

exports.signIn = async function (userData) {
  const { userEmail, userPassword } = userData;

  const user = await User.findOne({ email: userEmail }).exec();
  if (user) {
    const validPassword = await bcrypt.compare(userPassword, user.password);

    if (!validPassword) throw new Error("Password or email incorrect");

    const signResponse = new Promise((resolve, reject) => {
      jwt.sign(
        { user: userEmail },
        process.env.NODE_ENV_SECRET_KEY,
        (error, token) => {
          if (error) reject(error.message);

          resolve({
            user,
            token,
          });
        }
      );
    });

    return await signResponse;
  } else {
    throw new Error("Password or email incorrect");
  }
};

exports.forgot = async function (userData) {
  const { userEmail } = userData;
  const user = await User.findOne({ email: userEmail }).exec();

  if (!user) {
    throw new Error("User does not exists");
  } else {
    const generatePassword = () => {
      var length = 8,
        charset =
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
      for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
      }
      return retVal;
    };

    const newPassword = generatePassword();

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(newPassword, salt);

    user.password = hashPassword;

    const mailTransporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    let new_mail = await mailTransporter.sendMail({
      from: "Recuperar contraseña - Tesis drone app <kk099400@gmail.com>",
      to: userEmail,
      subject: `Nueva contraseña`,
      text: `Tu nuevo password es: ${newPassword}. No lo compartas con nadie.`,
    });
    console.log("Message sent: %s", new_mail.messageId);

    return await user.save();
  }
  r;
};

exports.passwordUpdate = async function (userData) {
  const { userEmail, userOldPassword, userNewPassword } = userData;

  const user = await User.findOne({ email: userEmail }).exec();

  if (user) {
    const validPassword = await bcrypt.compare(userOldPassword, user.password);
    if (!validPassword) throw new Error("Old password incorrect");

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(userNewPassword, salt);

    user.password = hashPassword;
    return await user.save();
  } else {
    throw new Error("User does not exists");
  }
};
