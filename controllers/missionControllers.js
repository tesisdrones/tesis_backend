const missionService = require("../services/missionService");

exports.getMission = async function (req, res) {
  try {
    const missionId = req.params["id"];
    const mission = await missionService.getMission(missionId);
    res.send(mission);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.searchAll = async function (req, res) {
  try {
    const missions = await missionService.searchAll();
    res.send(missions);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.searchAllMissionMobile = async function (req, res) {
  try {
    const missions = await missionService.searchAll();
    let mobileMissionsList = [];
    if (missions.length > 0) {
      mobileMissionsList = missions.map((mission) => {
        let number = 0;
        number++;

        const tpData = mission.path.trees.map((tree) => {
          const { waypoint, position, height } = tree;
          return {
            waypointCoordinates: [
              waypoint.coordinates[0],
              waypoint.coordinates[1],
            ],
            treeCoordinates: [position.coordinates[0], position.coordinates[1]],
            height: height,
          };
        });

        return {
          id: number,
          name: mission.name,
          routeData: tpData,
        };
      });
    } else {
      throw new Error("Missions list empty");
    }

    res.send(mobileMissionsList);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.create = async function (req, res) {
  try {
    const missionId = req.body["id"];
    const missionName = req.body["name"];
    const pathName = req.body["pathName"];
    const pathTrees = req.body["pathTrees"];
    const date = req.body["date"];

    const mission = await missionService.create(
      missionId,
      missionName,
      pathName,
      pathTrees,
      date
    );
    res.send(mission);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.execute = async function (req, res) {
  try {
    const missionId = req.params["id"];
    const mission = await missionService.execute(missionId);
    res.send(mission);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
