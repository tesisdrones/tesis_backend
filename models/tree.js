const moment = require("moment");
const mongoose = require("mongoose");
const { dateFormat } = require("../utils/constants");

const pointSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["Point"],
    required: true,
  },
  coordinates: {
    type: [Number],
    required: true,
  },
});

const treeSchema = new mongoose.Schema({
  position: {
    type: pointSchema,
  },
  waypoint: {
    type: pointSchema,
  },
  height: Number,
  index: {
    row: { type: Number, default: 0 },
    column: { type: Number, default: 0 },
  },
  variety: { type: String, required: true, default: "Nogal 1" },
  year: { type: Number, required: true, min: 2000, default: 2021 },
  images: [
    {
      image: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Image",
      },
    },
  ],
  lastScore: { type: Number, default: 0 },
  lastModifyDate: { type: String, default: moment().format(dateFormat) },
  tags: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Tag",
    },
  ],
});

treeSchema.virtual("lastImage").get(function () {
  return this.images[this.images.length - 1];
});

treeSchema.index({ waypoint: "2dsphere" });

const Tree = mongoose.model("Tree", treeSchema);

module.exports = { treeSchema, Tree };
