const Tag = require("../models/tag");
const { Tree } = require("../models/tree");

exports.searchAll = async function () {
  const tags = await Tag.find({}).exec();
  if (tags) {
    return tags;
  }
};

exports.create = async function (name, color) {
  let tag = await Tag.findOne({ name: name.newTag, color: name.color }).exec();
  if (!tag) {
    tag = new Tag({
      name: name.newTag,
      color: name.color
    });
    tag = await tag.save();
  } else {
    throw new Error("Tag already exists");
  }

  return tag;
};

exports.delete = async function (tag) {
  // Delete the Tag from all Trees
  const treesWithTag = await Tree.updateMany({ $pull: { tags: tag._id } });

  // Delete the tag from collection
  const tagDeleted = await Tag.deleteOne({ _id: tag._id }).exec();

  if (tagDeleted) {
    return { tag, treesAffected: treesWithTag.nModified };
  }
};
