const treesService = require("../services/treesService");
const { Image } = require("../models/image");

exports.create = async function (req, res) {
  try {
    const treeData = {
      position: req.body.position,
      waypoint: req.body.waypoint,
      variety: req.body.variety,
      year: req.body.year,
      row: req.body.row,
      column: req.body.column,
      height: req.body.height,
    };

    const tree = await treesService.create(treeData);

    res.send(tree);
  } catch (err) {
    console.log("err" + err);
    res.status(500).send(err.message);
  }
};

exports.search = async function (req, res) {
  try {
    const treeId = req.params["id"];
    const tree = await treesService.search(treeId);
    res.send(tree);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.searchAll = async function (req, res) {
  try {
    const tree = await treesService.searchAll();
    res.send(tree);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.delete = async function (req, res) {
  try {
    const treeId = req.params["id"];
    const tree = await treesService.delete(treeId);
    res.send(tree);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.update = async function (req, res) {
  try {
    const treeId = req.params["id"];
    const tree = await treesService.update(treeId);
    res.send(tree);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.countVariety = async function (req, res) {
  try {
    const tree = await treesService.countVariety();
    res.send(tree);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getImages = async function (req, res) {
  try {
    const treeId = req.params["id"];
    const tree = await treesService.getImages(treeId);
    res.send(tree);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.searchByFilter = async function (req, res) {
  try {
    const filters = req.body.filters;
    const trees = await treesService.searchByFilter(filters);
    res.send(trees);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getTreesByRow = async function (req, res) {
  try {
    const rowNumber = req.params["number"];
    const trees = await treesService.getByRow(rowNumber);

    if (trees.length > 0) {
      res.send(trees);
    } else {
      throw new Error(`No images for column ${rowNumber}`);
    }
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getAllVarieties = async function (req, res) {
  try {
    const treesVarieties = await treesService.getAllVarieties();

    res.send(treesVarieties);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getAvailableRows = async function (req, res) {
  try {
    const availableRows = await treesService.getAvailableRows();

    res.send(availableRows);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
