exports.convertExifGPSToDecimal = function(gpsData, direction) {
    var dd = gpsData[0] + gpsData[1]/60 + gpsData[2]/(60*60);
    if (direction == "S" || direction == "W") {
        dd = dd * -1;
    } // Don't do anything for N or E
    return dd;
}

