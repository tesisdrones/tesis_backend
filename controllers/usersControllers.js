const usersService = require("../services/usersService");

exports.search = async function (req, res) {
  try {
    const userId = req.params["id"];

    const user = await usersService.search(userId);

    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.searchAll = async function (req, res) {
  try {
    const user = await usersService.searchAll();

    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.delete = async function (req, res) {
  try {
    const userId = req.params["id"];

    const user = await usersService.delete(userId);

    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.signUp = async function (req, res) {
  try {
    const { name, surname, email, password } = req.body;

    const userData = {
      userName: name,
      userSurname: surname,
      userEmail: email,
      userPassword: password,
    };

    const user = await usersService.signUp(userData);
    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.signIn = async function (req, res) {
  try {
    const { email, password } = req.body;

    const userData = {
      userEmail: email,
      userPassword: password,
    };

    const user = await usersService.signIn(userData);

    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.forgot = async function (req, res) {
  try {
    const { email } = req.body;

    const userData = {
      userEmail: email,
    };

    const user = await usersService.forgot(userData);

    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.passwordUpdate = async function (req, res) {
  try {
    const { email, oldPassword, newPassword } = req.body;

    const userData = {
      userEmail: email,
      userOldPassword: oldPassword,
      userNewPassword: newPassword,
    };

    const user = await usersService.passwordUpdate(userData);

    res.send(user);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
