const mongoose = require("mongoose");

const imageSchema = new mongoose.Schema({
  mission_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Mission",
  },
  tree_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Tree",
  },
  url: { type: String },
  hash: { type: String },
  creationDate: { type: String },
  score: { type: Number, min: 1, max: 5, default: null },
  details: String,
});

const Image = mongoose.model("Image", imageSchema);

module.exports = {
  imageSchema,
  Image,
};
