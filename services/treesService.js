const { Tree } = require("../models/tree");

exports.create = async function (treeData) {
  let tree = await Tree.find({
    $or: [
      {
        "position.coordinates": [treeData.position.lat, treeData.position.lon],
        $and: [
          { "index.row": treeData.row },
          { "index.column": treeData.column },
        ],
      },
    ],
  }).exec();
  if (tree.length === 0) {
    tree = new Tree({
      position: {
        coordinates: [treeData.position.lat, treeData.position.lon],
        type: "Point",
      },
      waypoint: {
        coordinates: [treeData.waypoint.lat, treeData.waypoint.lon],
        type: "Point",
      },
      index: {
        row: treeData.row,
        column: treeData.column,
      },
      variety: treeData.variety,
      year: treeData.year,
      images: [],
      height: treeData.height,
    });
    tree = await tree.save();
  } else {
    throw new Error("Tree already exists");
  }

  return tree;
};

exports.search = async function (treeId) {
  let tree = await Tree.findOne({ _id: treeId })
    .populate("tags")
    .populate("images.image")
    .exec();
  if (tree) {
    return tree;
  }
};

exports.searchAll = async function () {
  let tree = await Tree.find({})
    .populate("tags")
    .populate("images.image")
    .exec();
  if (tree) {
    // }
    return tree;
  }
};

exports.delete = async function (treeId) {
  let tree = await Tree.deleteOne({ _id: treeId }).exec();
  if (tree) {
    return tree;
  }
};

exports.update = async function (treeId) {
  let tree = await Tree.update({ _id: treeId }).exec();
  if (tree) {
    return tree;
  }
};

exports.countVariety = async function () {
  let tree = await Tree.aggregate([
    { $group: { _id: "$variety", count: { $sum: 1 } } },
    { $sort: { count: 1 } },
  ]).exec();

  if (!tree) {
    return null;
  } else {
    return tree;
  }
};

exports.getImages = async function (treeId) {
  let tree = await Tree.findOne({ _id: treeId })
    .populate("images.image")
    .exec();

  if (tree) {
    return tree.images;
  }
};

exports.getTags = async function () {
  let tree = await Tree.findOne({ _id: treeId }).populate("tags.tag").exec();
  if (tree) {
    return tree.tags;
  }
};
// FILTERING TREES!!!!
exports.searchByFilter = async function (filters) {
  let trees = await Tree.find({ $and: filters })
    .populate("tags")
    .populate("images.image")
    .exec();
  if (trees) {
    return trees;
  }
};

exports.getByRow = async function (number) {
  let trees = await Tree.find({
    "index.row": number,
  });
  if (trees) {
    return trees;
  }
};

exports.getAllVarieties = async function () {
  let trees = await Tree.distinct("variety");

  if (trees) {
    return trees;
  }
};

exports.getAvailableRows = async function () {
  let trees = await Tree.distinct("index.row");

  if (trees) {
    return trees;
  }
};
