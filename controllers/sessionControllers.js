const jwt = require("jsonwebtoken");

exports.validateToken = (req, res, next) => {
  const header = req.headers["authorization"];
  const token = header && header.split(" ")[1];

  if (!token) {
    res.sendStatus(403);
  } else {
    jwt.verify(token, process.env.NODE_ENV_SECRET_KEY, (error) => {
      if (error) {
        res.sendStatus(403);
      } else {
        next();
      }
    });
  }
};
