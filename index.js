require("dotenv").config();
var express = require("express");

const fileUpload = require("express-fileupload");
const imagesController = require("./controllers/imagesControllers");
const treesController = require("./controllers/treesControllers");
const missionController = require("./controllers/missionControllers");
const usersController = require("./controllers/usersControllers");
const sessionController = require("./controllers/sessionControllers");
const tagsController = require("./controllers/tagsControllers");

const DB = require("./db/mongo");
const cors = require("cors");

DB.initDB();

var app = express();
app.use(fileUpload());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Endpoints

app.get("/", async function (req, res) {
  res.send(`Drone's flying!`);
});

//Users
app.get("/users", usersController.searchAll); // Get all users
app.get("/users/:id", usersController.search); // Search user by id
app.post("/users/SignUp", usersController.signUp); // Create new user
app.post("/users/SignIn", usersController.signIn); // Sign in a user
app.post("/users/forgot", usersController.forgot); // Sign in a user
app.put(
  "/users/password",
  sessionController.validateToken,
  usersController.passwordUpdate
); // Create new user
app.delete("/users/:id", usersController.delete); // Delete user by id

//Missions
app.get(
  "/missions",
  sessionController.validateToken,
  missionController.searchAll
); // Get all missions
app.get("/mobileMissions", missionController.searchAllMissionMobile); // Get all missions - mobile app version
app.get(
  "/missions/:id",
  sessionController.validateToken,
  missionController.getMission
); // Get mission by id
app.post(
  "/missions/:id/images",
  sessionController.validateToken,
  imagesController.upload
); // Upload single image by missionId
app.post(
  "/missions",
  sessionController.validateToken,
  missionController.create
); // Create mission
app.put("/missions/:id/execute", missionController.execute); // Update execute status of a mission

//Image
app.put(
  "/images/:id/score/:score",
  sessionController.validateToken,
  imagesController.updateScore
); // Rate image (score, details)

//Tags
app.get("/tags", sessionController.validateToken, tagsController.searchAll); // Get all tags
app.post("/tags", sessionController.validateToken, tagsController.create); // Create tag
app.delete("/tags", sessionController.validateToken, tagsController.delete); // Delete tag by name

//Trees
app.get("/trees", sessionController.validateToken, treesController.searchAll); // Get all trees
app.get("/trees/:id", sessionController.validateToken, treesController.search); // Get tree by id
app.get(
  "/trees/variety/counters",
  sessionController.validateToken,
  treesController.countVariety
); // Get all tree varieties and counts
app.post(
  "/trees/fields/filters",
  sessionController.validateToken,
  treesController.searchByFilter
); // Get all trees by fields (filter)
app.get(
  "/trees/:id/record",
  sessionController.validateToken,
  treesController.getImages
); // Get tree images by tree id
app.get(
  "/trees/path/rows",
  sessionController.validateToken,
  treesController.getAvailableRows
); // Get available rows
app.get(
  "/trees/row/:number",
  sessionController.validateToken,
  treesController.getTreesByRow
); // Get trees by row number
app.post("/trees", sessionController.validateToken, treesController.create); // Create tree
app.delete(
  "/trees/:id",
  sessionController.validateToken,
  treesController.delete
); // Delete tree

app.get(
  "/trees/variety/all",
  sessionController.validateToken,
  treesController.getAllVarieties
); // Get all trees varieties

//

app.listen(process.env.PORT || 3001, function () {
  console.log("App listening on port 3001");
});

app.use((req, res, next) => {
  const error = new HttpError("Could not find this route.", 404);
  throw error;
});

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({ message: "An unknown error occurred" });
});
