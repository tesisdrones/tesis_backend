const tagsService = require("../services/tagsService");

exports.searchAll = async function (req, res) {
  try {
    const tags = await tagsService.searchAll();
    res.send(tags);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.create = async function (req, res) {
  try {
    const tagName = req.body.name;
    const tag = await tagsService.create(tagName);
    res.send(tag);
  } catch (err) {
    console.log("err" + err);
    res.status(500).send(err.message);
  }
};

exports.delete = async function (req, res) {
  try {
    const tagName = req.body.name;
    const tag = await tagsService.delete(tagName);
    res.send(tag);
  } catch (err) {
    res.status(500).send(err.message);
  }
};
