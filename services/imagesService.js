const AWS = require("aws-sdk");
const md5 = require("md5");
const moment = require("moment");

const { Image } = require("../models/image");
const { Tree } = require("../models/tree");
const Mission = require("../models/mission");
const { dateFormat } = require("../utils/constants");

const bucketName = process.env.BUCKET_NAME;
const ACL_PUBLIC_READ = "public-read";

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
});

var s3 = new AWS.S3();

exports.upload = async function (missionId, imageId, img, lat, lon) {
  const hash = md5(img.data);

  //Find current mission
  let mission = await Mission.findOne({ id: missionId })
    .populate("images.image")
    .exec();

  //If image is not in mission add it and save it
  if (!mission.images.some((e) => e.image && e.image.hash === hash)) {
    var S3params = {
      Bucket: bucketName,
      Key: `${missionId}/${hash}`,
      Body: img.data,
      ACL: ACL_PUBLIC_READ,
      ContentType: "image/jpg",
    };

    const S3res = await s3.upload(S3params).promise();

    //Create new image object
    const image = new Image({
      id: imageId,
      mission_id: mission,
      url: S3res.Location,
      hash: hash,
      creationDate: mission.date,
    });

    await matchWithTree(image, lat, lon);
    await image.save();

    mission.images.push({
      image: image,
    });
    mission = await mission.save();
  }

  //TODO: buscar una mejor forma de evitar un max call stack size exceeded
  //no pudimos retornar el objeto mission porque genera un max call stack size exceeded al autoreferenciarse
  return await Mission.findOne({ id: missionId })
    .populate("images.image")
    .exec();
};

exports.score = async function (imageId, score, details, tags) {
  const image = await Image.findOne({ _id: imageId }).exec();

  if (!image) {
    return null;
  } else {
    if (image.tree_id) {
      const tree = await Tree.findOne({ _id: image.tree_id }).exec();
      tree.lastScore = score;
      tree.tags = tags;
      tree.lastModifyDate = moment().format(dateFormat);

      await tree.save();
    }

    image.score = score;
    image.details = details;
    image.tags = tags;

    return await image.save();
  }
};

//Esta logica podría ejecutarse de forma async usando colas de mensajes y un sistema un poco mas armado
async function matchWithTree(image, lat, lon) {
  const tree = await Tree.findOne({
    waypoint: {
      $near: {
        $maxDistance: 5,
        $geometry: { type: "Point", coordinates: [lat, lon] },
      },
    },
  }).exec();

  if (tree) {
    image.tree_id = tree;
    tree.images.push({
      image: image,
    });
    tree.lastScore = image.score;
    tree.lastModifyDate = moment().format(dateFormat);
    await image.save();
    await tree.save();
  } else {
    throw new Error("Cannot match image with a existing Tree");
  }
}
